package com.example.lesson11.service;

import com.example.lesson11.dto.StudentTeacherSaveDTO;
import com.example.lesson11.mapper.StudentTeacherMapper;
import com.example.lesson11.model.StudentTeacher;
import com.example.lesson11.service.api.StudentTeacherService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StudentTeacherServiceImpl implements StudentTeacherService {
    private final ModelMapper mapper = new ModelMapper();

    @Autowired
    private StudentTeacherMapper studentTeacherMapper;

    public void save(StudentTeacherSaveDTO saveDTO){
        StudentTeacher studentTeacher =  mapper.map(saveDTO, StudentTeacher.class);
        studentTeacherMapper.save(studentTeacher);
    }

    public void delete(StudentTeacherSaveDTO deleteDTO) {
        StudentTeacher studentTeacher = mapper.map(deleteDTO, StudentTeacher.class);
        studentTeacherMapper.delete(studentTeacher);
    }
}
