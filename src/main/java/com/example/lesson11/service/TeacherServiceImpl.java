package com.example.lesson11.service;

import com.example.lesson11.dto.TeacherSaveDTO;
import com.example.lesson11.mapper.TeacherMapper;
import com.example.lesson11.model.Teacher;
import com.example.lesson11.service.api.TeacherService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TeacherServiceImpl implements TeacherService {
    private final ModelMapper mapper = new ModelMapper();

    @Autowired
    private TeacherMapper teacherMapper;

    @Override
    public List<Teacher> getAll() {
        return teacherMapper.getAll();
    }

    public void save(TeacherSaveDTO saveDTO){
        Teacher teacher =  mapper.map(saveDTO, Teacher.class);
        teacherMapper.save(teacher);
    }

    public void update(TeacherSaveDTO updateDTO){
        Teacher teacher =  mapper.map(updateDTO, Teacher.class);
        teacherMapper.update(teacher);
    }

    public void delete(TeacherSaveDTO deleteDTO){
        Teacher teacher =  mapper.map(deleteDTO, Teacher.class);
        teacherMapper.delete(teacher);
    }
}
