package com.example.lesson11.service;

import com.example.lesson11.dto.StudentSaveDTO;
import com.example.lesson11.mapper.StudentMapper;
import com.example.lesson11.model.Student;
import com.example.lesson11.service.api.StudentService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentServiceImpl implements StudentService {
    private final ModelMapper mapper = new ModelMapper();

    @Autowired
    private StudentMapper studentMapper;

    @Override
    public List<Student> getAll() {
        return studentMapper.getAll();
    }

    public void save(StudentSaveDTO saveDTO){
        Student student =  mapper.map(saveDTO, Student.class);
        studentMapper.save(student);
    }

    public void update(StudentSaveDTO updateDTO){
        Student student =  mapper.map(updateDTO, Student.class);
        studentMapper.update(student);
    }

    public void delete(StudentSaveDTO deleteDTO) {
        Student student = mapper.map(deleteDTO, Student.class);
        studentMapper.delete(student);
    }
}
