package com.example.lesson11.service.api;

import com.example.lesson11.dto.TeacherSaveDTO;
import com.example.lesson11.model.Teacher;

import java.util.List;

public interface TeacherService {
    List<Teacher> getAll();

    void save(TeacherSaveDTO saveDTO);
    void update(TeacherSaveDTO updateDTO);
    void delete(TeacherSaveDTO deleteDTO);
}
