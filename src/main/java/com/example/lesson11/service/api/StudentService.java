package com.example.lesson11.service.api;

import com.example.lesson11.dto.StudentSaveDTO;
import com.example.lesson11.model.Student;

import java.util.List;

public interface StudentService {
    List<Student> getAll();

    void save(StudentSaveDTO saveDTO);
    void update(StudentSaveDTO updateDTO);
    void delete(StudentSaveDTO deleteDTO);
}
