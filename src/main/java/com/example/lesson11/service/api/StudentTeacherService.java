package com.example.lesson11.service.api;

import com.example.lesson11.dto.StudentTeacherSaveDTO;

public interface StudentTeacherService {
    void save(StudentTeacherSaveDTO studentTeacherSaveDTO);
    void delete(StudentTeacherSaveDTO studentTeacherSaveDTO);
}
