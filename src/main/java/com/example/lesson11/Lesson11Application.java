package com.example.lesson11;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.example.lesson11.mapper")
public class Lesson11Application {

    public static void main(String[] args) {
        SpringApplication.run(Lesson11Application.class, args);
    }

}
