package com.example.lesson11.dto;

import lombok.Data;

@Data
public class TeacherSaveDTO {
    private Integer id;
    private String fio;
    private String department;
}
