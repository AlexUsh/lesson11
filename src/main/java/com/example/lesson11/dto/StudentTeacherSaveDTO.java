package com.example.lesson11.dto;

import lombok.Data;

@Data
public class StudentTeacherSaveDTO {
    private Long studentId;
    private Long teacherId;
}
