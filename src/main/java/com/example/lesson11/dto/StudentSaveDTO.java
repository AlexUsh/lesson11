package com.example.lesson11.dto;

import lombok.Data;

@Data
public class StudentSaveDTO {
    private Integer id;
    private String fio;
    private String specialty;
    private Integer course;
}
