package com.example.lesson11.mapper;

import com.example.lesson11.model.Student;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface StudentMapper {
    @Select("select * from student")
    List<Student> getAll();

    @Insert("insert into student(fio, specialty, course)" +
            "values(#{l.fio}, #{l.specialty}, #{l.course})")
    void save(@Param("l") Student student);

    @Update("update student set fio = #{l.fio}, specialty = #{l.specialty}, course = #{l.course}")
    void update(@Param("l") Student student);

    @Delete("delete from student where id = #{l.id}")
    void delete(@Param("l") Student student);
}
