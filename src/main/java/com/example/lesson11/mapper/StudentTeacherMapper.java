package com.example.lesson11.mapper;

import com.example.lesson11.model.StudentTeacher;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface StudentTeacherMapper {
    @Insert("insert into student_teacher(student_id, teacher_id)" +
            "values(#{l.studentId} ,#{l.teacherId})")
    void save(@Param("l") StudentTeacher studentTeacher);

    @Delete("delete from student_teacher where student_id=#{l.studentId} and teacher_id=#{l.teacherId}")
    void delete(@Param("l") StudentTeacher studentTeacher);
}
