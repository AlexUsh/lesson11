package com.example.lesson11.mapper;

import com.example.lesson11.model.Teacher;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface TeacherMapper {
    @Select("select * from teacher")
    List<Teacher> getAll();

    @Insert("insert into teacher(fio, department)" +
            "values(#{l.fio}, #{l.department})")
    void  save(@Param("l") Teacher teacher);

    @Update("update student set fio = #{l.fio}, department = #{l.department}")
    void update(@Param("l") Teacher teacher);

    @Delete("delete from student where id = #{l.id}")
    void delete(@Param("l") Teacher teacher);
}
