package com.example.lesson11.controller;

import com.example.lesson11.dto.TeacherSaveDTO;
import com.example.lesson11.model.Teacher;
import com.example.lesson11.service.api.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/teacher")
public class TeacherController {
    @Autowired
    private TeacherService teacherService;

    @GetMapping
    public List<Teacher> getAll() {
        return teacherService.getAll();
    }

    @PostMapping("/save")
    public void save(@RequestBody TeacherSaveDTO saveDTO){
        teacherService.save(saveDTO);
    }

    @PostMapping("/update")
    public void update(@RequestBody TeacherSaveDTO updateDTO){
        teacherService.update(updateDTO);
    }

    @PostMapping("/delete")
    public void delete(@RequestBody TeacherSaveDTO deleteDTO){
        teacherService.delete(deleteDTO);
    }
}
