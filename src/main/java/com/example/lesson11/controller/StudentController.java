package com.example.lesson11.controller;

import com.example.lesson11.dto.StudentSaveDTO;
import com.example.lesson11.model.Student;
import com.example.lesson11.service.api.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/student")
public class StudentController {
    @Autowired
    private StudentService studentService;

    @GetMapping
    public List<Student> getAll() {
        return studentService.getAll();
    }

    @PostMapping("/save")
    public void save(@RequestBody StudentSaveDTO saveDTO){
        studentService.save(saveDTO);
    }

    @PostMapping("/update")
    public void update(@RequestBody StudentSaveDTO updateDTO){
        studentService.update(updateDTO);
    }

    @PostMapping("/delete")
    public void delete(@RequestBody StudentSaveDTO deleteDTO){
        studentService.delete(deleteDTO);
    }
}
