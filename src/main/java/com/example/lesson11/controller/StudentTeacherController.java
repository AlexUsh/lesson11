package com.example.lesson11.controller;

import com.example.lesson11.dto.StudentTeacherSaveDTO;
import com.example.lesson11.service.api.StudentTeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/student-teacher")
public class StudentTeacherController {
    @Autowired
    private StudentTeacherService studentTeacherService;

    @PostMapping("/save")
    public void update(@RequestBody StudentTeacherSaveDTO saveDTO){
        studentTeacherService.save(saveDTO);
    }

    @PostMapping("/delete")
    public void delete(@RequestBody StudentTeacherSaveDTO deleteDTO){
        studentTeacherService.delete(deleteDTO);
    }
}
