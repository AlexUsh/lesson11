package com.example.lesson11.model;

import lombok.Data;

@Data
public class Teacher {
    private Integer id;
    private String fio;
    private String department;
}
