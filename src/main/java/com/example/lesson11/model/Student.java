package com.example.lesson11.model;

import lombok.Data;

@Data
public class Student {
    private Integer id;
    private String fio;
    private String specialty;
    private Integer course;
}
