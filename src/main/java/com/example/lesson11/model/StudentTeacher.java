package com.example.lesson11.model;

import lombok.Data;

@Data
public class StudentTeacher {
    private Long student_id;
    private Long teacher_id;
}
