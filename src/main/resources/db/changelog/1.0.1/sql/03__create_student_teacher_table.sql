create table student_teacher(
    student_id bigint not null references student(student_id),
    teacher_id bigint not null references teacher(teacher_id)
)