CREATE TABLE teacher (
    id          INTEGER NOT NULL ,
    fio         VARCHAR(30) NOT NULL ,
    department  VARCHAR(30) NOT NULL ,
    PRIMARY KEY (id)
);