CREATE TABLE student (
    id          INTEGER NOT NULL ,
    fio         VARCHAR(30) NOT NULL ,
    specialty   VARCHAR(35) NOT NULL ,
    course      INTEGER NOT NULL ,
    PRIMARY KEY (id)
);